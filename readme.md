# For The Record Coding Test Documentation

This project is an implementation of the requirements set out in the FTR Coding Test.  

## Overview

The design is based on a simple command line nodejs program with associated classes that implement the core functions of the program. 

### Assumptions
- No persistence of data between executions;
- Only single user of the data on a local workstation only ie. not deployed to a server for consumption remotely by multiple users.

## Project Setup

Clone or download the code base from gitlab into a local directory.
Ensure you have Node and NPM installed on your local workstation.  Open a terminal window and change directory to root of the project.  Execute the following to install the required node modules.

```
npm install --g
npm run build
```

## Run the project

The application can be run using npm scripts or can be executed directly from /usr/local/bin
```
npm run start
```
or
```
cd /usr/local/bin
ftr
```

## Design

This business logic for this application is encapsulated within the Analytics class.  The basic functionality enables an application to:
- Define the interval by which analytics data is published;
- Add numbers to the Analytics object that will capture the number of times the number has passed in;
- Allow an application to register a listener (AnalyticsHandler) that will be notified at the specified interval;
- Allow an application to pause and resume the publication of analytics data from the Analytics object;
- Publish the analytics data as a simple Map of key/value pairs that can be consumed and presented as required by the client application (in this case a console app printing out a list).

# Future Changes to Application

## New UI
The design of the core application logic encapsulated within the Analytics.ts file will enable the re-use of the logic over various client types.

This Analytics class could be used directly within another project such as an Angular project to build a web based SPA but this again would be single user on the local browser only.

If further requirements were received for multi-user, persistence etc. then the business logic would need to be redefined possibly within a REST service deployed to a service container such as Docker/K8s.

## Production Readiness

To setup for production readiness the build pipeline will need to include the following:
- Automated Unit testing
- Test coverage report
- Deployment (in this case is only deployed as a local console application so needs to be installed locally).

## What do I think of the test

Test was enough to stretch but was small enough to ensure I could get done in a reasonable time.  I chose to build a node console app rather than web UI so that was a bit different.

I think the spec could include the evaluation criteria to ensure the participant concentrates on areas important to FTR.

I could have spent a week building a full blown SPA deployed into AWS or Firebase but understood this was not what was desired.

All in all a good experience!



