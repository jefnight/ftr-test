#!/usr/bin/env node
import readline from "readline";
import { Analytics, AnalyticsHandler } from "./analytics";

/**
 * Implementation of AnalyticsHandler interface to handle the 
 * notifiication of the analytics data at the specified interval.
 * In this case logs the data into a format suitable for logging to console.
 */
class AnalyticsWriter implements AnalyticsHandler {
  onAnalytics(analytics: Map<number, number>) {
    this.writeStats(analytics);
  }

   writeStats(statsMap: Map<number, number>) {
    let statsStr = new Array<string>();
    statsMap.forEach((value, key) => {
      statsStr.push(key + ":" + value);
    });
    console.log(statsStr.join(","));
  }
}

/**
 * Setup the storage and logic for holding the analytics data 
 * collected from user input at the console.
 */
let analytics = new Analytics();
let analyticsWriter = new AnalyticsWriter();
analytics.analyticsHandler = analyticsWriter;

/**
 * Main console application that will prompt for user input
 * for ingestion into Analytics object.  Also uses the available methods in
 * the Analytics to control the publishing of the data.
 */
async function run() {
  let rl = readline.createInterface(process.stdin, process.stdout);
  let currentCmd = "freq";
  rl.setPrompt(
    ">> Please input the time in seconds between emitting numbers and their frequency \n"
  );
  rl.on("line", (line) => {
    if (line === "quit") {
      analyticsWriter.writeStats(analytics.getStats());
      rl.close();
    } else if (line === "halt") {
      analytics.isPublishing(false);
    } else if (line === "resume") {
      analytics.isPublishing(true);
    } else {
      switch (currentCmd) {
        case "freq":
          analytics.setPublishFrequency(+line);
          rl.setPrompt(">> Please enter first number\n");
          currentCmd = "first";
          break;
        case "first":
          currentCmd = "next";
          rl.setPrompt(">> Please enter the next number\n");
          if (!Number.isNaN(parseInt(line))) {
            console.log(analytics.add(parseInt(line)));
          } else {
            console.log("NaN please try again\n");
          }
          break;
        case "next":
          currentCmd = "next";
          if (!Number.isNaN(parseInt(line))) {
            console.log(analytics.add(parseInt(line)));
          } else {
            console.log("NaN please try again\n");
          }
          break;
      }
    }

    rl.prompt();
  }).on("close", () => {
    console.log("\n>> Thanks for playing. Come back soon!");
    process.exit(0);
  });
  rl.prompt();
}

run();
