/**
 * Interface definiton that listening client applications
 * will implement to receive notifications of analytics updates 
 * at specified interval.
 */
export interface AnalyticsHandler {
    onAnalytics(analytics: Map<number, number>): void;
}

/**
 * Analytics class that is instantiated by client applications that encapsulates
 * the logic of collecting and disemminating the analytics data.
 */
export class Analytics {
    private _numberStats = new Map<number, number>();
    private _fibSeries: number[] = [0,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987];
    private _timer: any;
    private _handler: AnalyticsHandler | undefined;
    private _publishFrequency: number = 0;

    constructor(){};

    /**
     * Client application will register an AnalyticsHandler class 
     * to callback with analytics data.
     */
    set analyticsHandler(aHandler: AnalyticsHandler){
        this._handler = aHandler; 
    }

    /**
     * Allows clients to pause and resume publishing of analytics data.
     * @param flag True to restart publish at previously set frequency
     */
    isPublishing(flag: boolean){
        flag ? this.setPublishFrequency(this._publishFrequency): clearTimeout(this._timer);
    }

    /**
     * Allows clients to set the frequency in seconds of the inteerval that
     * analyitics data will be published.  When the interval is fired the 
     * registered AnalyticsHandler will be notified of all analytics data.
     * @param frequency Number of seconds
     * 
     */
    setPublishFrequency(frequency: number){
        this._publishFrequency = frequency;
        this._timer = setInterval(() => {
            this._handler?.onAnalytics(this.getStats());
        }, frequency*1000);
    }

    /**
     * Adds a number to the analytics list and increases the count of the 
     * number of times the number has been added.  Reeturns a message if number
     * is part of the fibonacci sequence below 1000.
     * @param input 
     */
    add(input: number): string{
        let count:number = this._numberStats.get(input) || 0;
        let message: string = '';
        this._numberStats.set(input, count+=1);
        this._fibSeries.includes(input) ? message = 'FIB': null;
        return message;
    }

    /**
     * Used to query for the analytics data rather than be notified at the 
     * speecified interval.  Ensures map of data is sorted from highest 
     * occurrence to lowest. 
     */
    getStats(): Map<number, number>{
        let sortedMap = new Map<number, number>([...this._numberStats.entries()].sort((a,b) => b[1] - a[1]));
        return sortedMap;
    }
}